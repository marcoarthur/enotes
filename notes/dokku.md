# Dokku your own PaaS inside Subutai

This article is a very simple but nice idea about two cloud projects and
a curious application that emerges from direct use of both. 

![Matryoshka doll](https://upload.wikimedia.org/wikipedia/commons/thumb/4/41/Floral_matryoshka_set_2_smallest_doll_nested.JPG/640px-Floral_matryoshka_set_2_smallest_doll_nested.JPG)<sub> from Wikipedia's [Matryoshka doll].</sub>

# The main theme: Containers

Docker is well known in the container world. And largely adopted in main stream
cloud world. Subutai is too a cloud platform created with the intention to be
a peer-to-peer (p2p) cloud, so users can offer computer resources and manage
them in a bazaar made concrete by the central [ bazaar site
](https://bazaar.subutai.io/)

Subutai implements their container thing using LXC/LXD technology. And many
other p2p components in order to orchestrate the entire operations. As LXC/LXD
can be conceptually more closely related to virtual machine. We have them more
closely related as a private server ( see [ OpenVZ ](https://openvz.org/) ).
So, after we have a Subutai container deployed we can easily log into it using
ssh and start configuring the server for our needs, as you would do with a
Virtual Private Server (VPS).

That said, we start building our cloud infrastructure application inside our
cloud infrastructure of our own made possible by Subutai.

# Virtualisation Inside Virtualisation: Nesting

Nested virtualisation is a thing. A main thing. We have virtual machines, using
hypervisors such as kvm/quemu, vmware, etc.. We have virtualisation with containers,
docker, lxd/lxc, openVz, and we have chroot, jailing as the primitives forms
of virtualisations. Everyone can play and usually is played inside one another.
The **indirection** theme is one diverse playful theme as it seems.

The key feature is always to allow more with the same, but these of course
comes with more overhead and a difficult management, the tools becomes complex
to handle a myriad of problems and sometimes we can get lost trying to see the
debug behind deeper layers.

But, undoubtedly is really nice to make use of virtualisation tools. Once you
get used this is really good way to separate and organize applications and
resources. Providing security, and improving the utilization of the resources.


# Subutai: our first layer

[Subutai](http://subutai.io/) platform offers a nice interplay with Lxc/Lxd
. It is really simple to use it. And is deadly easy to participate and run.
I invite the reader to [follow the
instructions](https://subutai.io/getting-started.html) and setup your own
resource host and subscribe to [bazaar](https://bazaar.subutai.io/). Subutai
has a lot of users around the world and growing community that mostly can be
found in [slack](https://subutai-io.slack.com/). So check it and have fun.

<!--TODO: Subutai image logo and printscreen with the war room -->

# Dokku inside Subutai: The second layer

[Dokku](http://dokku.viewdocs.io/dokku/getting-started/installation/) is well
known as the "smallest PaaS in the world" and a really cool tool if you like to
have your application in docker as all the world now seems to like. And it is
used by lot of developers so they can ship their application to some test environment
where they can fully control. And for the ones that already played with heroku
platform the mindset is really the same, every git push triggers a build for
the application, and will be made available for tests, there is also lots of
plugins to setup database, queue system, etc..

# Dokku in action: Deploying our application in a container

Now we can see Dokku in action and deploy our first application. As we already
know as developers we will be building an application probably to be shipped
for the web, where users can access with their browsers. For this application
I will make a very simple one because we won't need to focus on it, but on the
simple process to ship it in Dokku.

# Grand finalle, The glue: Subutai blueprint for Dokku.

The manual process with Subutai and also with Dokku can be glued in one
document a Subutai BluePrint. This makes deadly simple by using Ansible to
deploy the Dokku to in a Subutai container and configure it.  The BluePrint
will set variables for configuration, in this case the,  simple hostname,
domain name and Dokku ssh port. So these two previous steps can become one
single click after a form completion.
 
<!-- image for BluePrint and link to github  and bazaar link-->


With this set we can know run Dokku in Subutai cloud.

# Conclusion, an after thought

We have organized two Nested virtualisation scheme, but in reality there is more.
Since, Subutai itself maybe runnning under a Virtual Machine under the laptop
of someone, or even in a virtual environment in a big cloud infrastructure,
we can only guess.

We setup a container in Subutai (manually or by the BluePrint means) and we
setup Dokku inside. And we now have two virtual environments to manage: virtual
environment of Subutai and its containers (lxc/lxd) and the virtual environment
of Dokku and its containers (docker). Each one  has its own set of tools, for
the Subutai we found them in bazaar and in the control management tool delivery
for Subutai users. For Dokku we found it with docker command line and in Dokku
command line client tool.

In this simple example we see close two clearly problems that affects Nested
Virtualisation: 

1. The performance issues due to the overhead and indirection levels. In the
   example Dokku performs badly, mostly because the docker storage used is vfs
   and we have no other options, because we would need to have the privileges
   in the subutai host to allow the use of other drivers and the attachment of
   any physical disk device, such as zfs storage driver or even overlay2 (the
   default for docker)

2. The complex management of both layers. We are not completely able to manage
   the two layers at the same like as transparent layer. Exposing the port of
   the dokku applications deployments and the subutai port mapping, for example. This
   problem could be handled but needs manual intervention. A more general solution
   is to coordinate trigger a hook from the Dokku to create this port mapping
   using the REST subutai bazaar interface. That shouldn't be difficult and would
   ensure a better experience when Dokku is running under Subutai cloud infra.

There are many research on topic of virtualisation, security being one of the
most intense, since there are many threats for any virtual environment. p2p
cloud is really a hot topic and Subutai is by far the most prominent
application in this field. I invite the user to read more about those topics
in general following the little bibliography I listed at the end of this post.

Thanks and let me know your thoughts and questions.

# Bibliography and References
[ Matryoshka doll ]: https://en.wikipedia.org/wiki/Matryoshka_doll
